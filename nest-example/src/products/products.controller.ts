import { Controller, Post, Body, Get, Param, Patch, Delete } from '@nestjs/common';
import { ProductsService } from './products.service';

@Controller('products')
export class ProductsController {
  constructor(private readonly productsService: ProductsService) {}
  
  /* only first step, if array is empty*/
  @Post('setfirst')
  setFirstProducts() {
    this.productsService.insertProductSamples();
    return "First step runned."
  }

  @Post('insert')
  addProduct(
    @Body('name') prodName: string,
    @Body('price') prodPrice: number,
    @Body('description') prodDescription: string
  ): {id: string} {
    return this.productsService.insertProduct(prodName, prodPrice, prodDescription);
  }

  @Get()
  getAllProducts(): any {
    return this.productsService.getAllProducts();
  }

  @Get(':id')
  getProducts(@Param('id') prodId: string): any {
    return this.productsService.getProduct(prodId);
  }

  @Patch(':id')
  updateProduct(
    @Param('id') prodId: string,
    @Body('name') prodName: string,
    @Body('price') prodPrice: number,
    @Body('description') prodDesc: string
  ): any {
    this.productsService.updateProduct(prodId, prodName, prodPrice, prodDesc);
    return 'update ok';
  }

  @Delete(':id')
  removeProduct(@Param('id') prodId: string): any {
    this.productsService.deleteProduct(prodId);
    return 'remove ok';
  }
}
