import { Injectable, NotFoundException } from '@nestjs/common';

import { Product } from './product.model';

@Injectable()
export class ProductsService {
  products: Product[] = [];

  insertProduct(name: string, price: number, desc: string): {id: string} {
    let newIndex = this.products.length;
    while(this.products.find(prod => prod.id === newIndex.toString()) !== undefined) {
      newIndex++;
    }
    const prodId = newIndex.toString();
    const newProd = new Product(prodId, name, price, desc);
    this.products.push(newProd);
    return {id: prodId};
  }
  
  /* only first step, if array is empty*/
  insertProductSamples(): void {
    this.products.push(new Product(this.products.length.toString(), 'Pineapple', 2.0, 'fruit'));
    this.products.push(new Product(this.products.length.toString(), 'Apple', 1.2, 'fruit'));
    this.products.push(new Product(this.products.length.toString(), 'Tomato', 2.1, 'vegetable'));
    this.products.push(new Product(this.products.length.toString(), 'Potato', 0.6, 'vegetable'));
    this.products.push(new Product(this.products.length.toString(), 'Grape', 2.5, 'fruit'));
    this.products.push(new Product(this.products.length.toString(), 'Pear', 1.5, 'fruit'));
    this.products.push(new Product(this.products.length.toString(), 'Onion', 0.6, 'vegetable'));
  }

  getAllProducts(): Array<Product> {
    return [ ...this.products ];
  }

  getProduct(id: string): Product {
    const [product, productIndex] = this.findProduct(id);
    return { ...product };
  }

  updateProduct(id: string, name: string, price: number, desc: string) {
    const [product, productIndex] = this.findProduct(id);
    const updatedProduct = {...product};
    if (name) {
      updatedProduct.name = name;
    }
    if (price) {
      updatedProduct.price = price;
    }
    if (desc) {
      updatedProduct.description = desc;
    }
    this.products[productIndex] = updatedProduct;
  }

  deleteProduct(id: string) {
    const [product, productIndex] = this.findProduct(id);
    this.products.splice(productIndex, 1);
  }

  private findProduct(id: string): [Product, number] {
    const productIndex = this.products.findIndex(prod => prod.id === id);
    const product = this.products.find(prod => prod.id === id);
    if (!product) {
      throw new NotFoundException('Could not find product.');
    }
    return [product, productIndex];
  }
}
